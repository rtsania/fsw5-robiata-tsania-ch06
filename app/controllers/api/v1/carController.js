const carService = require("../../../services/carService");

module.exports = {
    list(req, res) {
        carService
            .list({
                where: { isDeleted: false },
            })
            .then(({ data, count }) => {
                res.status(200).json({
                    status: "OK",
                    message: "Get All Cars Success",
                    data: { cars: data },
                    meta: { total: count },
                });
            })
            .catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    create(req, res) {
        console.log(req.body);
        req.body.createdBy = req.user.id;
        carService
            .create(req.body)
            .then((car) => {
                res.status(201).json({
                    status: "OK",
                    message: "Create Car Success",
                    data: car,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    update(req, res) {
        req.body.updatedBy = req.user.id;
        carService
            .update(req.params.id, req.body)
            .then(() => {
                res.status(200).json({
                    status: "OK",
                    message: "Update Car Success",
                    data: req.body
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    show(req, res) {
        carService
            .get(req.params.id)
            .then((post) => {
                res.status(200).json({
                    status: "OK",
                    message: "Get Car By Id Success",
                    data: post,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    makeCarDeleted(req, res) {
        carService
            .isCarDeleted(req.params.id, { isDeleted: true, deletedBy: req.user.id })
            .then((car) => {
                res.status(200).json({
                    status: "OK",
                    message: "Delete Car Success",
                    deletedBy: req.user.id,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    makeCarDestroy(req, res) {
        carService
            .delete(req.params.id)
            .then((car) => {
                res.status(200).json({
                    status: "OK",
                    message: "Car Destroy From Database",
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    }
};
